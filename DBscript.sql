USE [structureProject]
GO
/****** Object:  Table [dbo].[BetBuilderSelections]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BetBuilderSelections](
	[BetBuilderSelectionId] [int] NOT NULL,
	[BetSelectionId] [int] NOT NULL,
 CONSTRAINT [PK_BetBuilderSelections] PRIMARY KEY CLUSTERED 
(
	[BetBuilderSelectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bets]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bets](
	[ReSettle] [bit] NULL,
	[ManualSettle] [bit] NULL,
	[Bonus] [decimal](19, 2) NULL,
	[SkipLimits] [bit] NULL,
	[BetProduct] [int] NULL,
	[BonusPart] [decimal](19, 2) NULL,
	[SettlementStatus] [int] NULL,
	[SelectionsCount] [int] NULL,
	[FeedSourceId] [int] NULL,
	[BonusMode] [int] NULL,
	[TotalStakeBase] [decimal](19, 2) NULL,
	[PotWinBase] [decimal](19, 2) NULL,
	[AutoSettle] [bit] NULL,
	[RealWinBase] [decimal](19, 2) NULL,
	[HasPartialCashout] [bit] NULL,
	[TaxType] [int] NULL,
	[TaxValue] [decimal](19, 2) NULL,
	[BetTransactionId] [int] NOT NULL,
	[BonusPartBase] [decimal](19, 2) NULL,
	[RealWin] [decimal](19, 2) NULL,
	[BetId] [int] NOT NULL,
	[PlayerId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[BetStatus] [int] NULL,
	[BetType] [int] NULL,
	[UnitStake] [decimal](19, 2) NULL,
	[TotalStake] [decimal](19, 2) NULL,
	[OpenStake] [decimal](19, 2) NULL,
	[PotWin] [decimal](19, 2) NULL,
	[OpenPotWin] [decimal](19, 2) NULL,
	[MaxWinnings] [decimal](19, 2) NULL,
	[EventsCount] [int] NULL,
	[SettlementDate] [datetime] NULL,
	[CurrencyId] [int] NULL,
	[CurrencyRate] [decimal](19, 8) NULL,
	[IsLocked] [bit] NULL,
	[ModifiedDate] [datetime] NULL,
	[LicenseeId] [int] NULL,
 CONSTRAINT [PK_Bets] PRIMARY KEY CLUSTERED 
(
	[BetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BetSelections]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BetSelections](
	[ExistBetsValue] [decimal](19, 10) NULL,
	[SelectionTypeId] [int] NULL,
	[SportMarketId] [int] NULL,
	[SportId] [int] NULL,
	[SportName] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[ChampId] [int] NULL,
	[MarketName] [nvarchar](max) NULL,
	[IsLive] [bit] NULL,
	[IsVirtual] [bit] NULL,
	[State] [int] NULL,
	[IsBetBuilder] [bit] NULL,
	[IsParlay] [bit] NULL,
	[IsLiveStream] [bit] NULL,
	[FeedSourceId] [int] NULL,
	[DbId] [int] NULL,
	[VSportGameId] [int] NULL,
	[EventFactorValue] [decimal](19, 10) NULL,
	[EventDate] [datetime] NULL,
	[SettlOt] [bit] NULL,
	[BetSelectionId] [int] NOT NULL,
	[BetId] [int] NOT NULL,
	[IsBanker] [bit] NULL,
	[BetProduct] [int] NULL,
	[FeedSelectionId] [int] NULL,
	[FeedEventId] [int] NULL,
	[IntEventId] [int] NULL,
	[Price] [decimal](19, 10) NULL,
	[PriceType] [int] NULL,
	[Status] [int] NULL,
	[MarketType] [int] NULL,
	[EventDesc] [nvarchar](max) NULL,
	[ChampDesc] [nvarchar](max) NULL,
	[CatDesc] [nvarchar](max) NULL,
	[SelectionName] [nvarchar](max) NULL,
	[SelectionCode] [nvarchar](max) NULL,
	[Spec] [nvarchar](max) NULL,
	[FeedMarketId] [int] NULL,
 CONSTRAINT [PK_BetSelections] PRIMARY KEY CLUSTERED 
(
	[BetSelectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BetTransaction]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BetTransaction](
	[BetTransactionId] [int] NOT NULL,
	[BetsCount] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[BonusMeta] [nvarchar](max) NULL,
	[ClientIp] [nvarchar](max) NULL,
	[Device] [int] NULL,
	[LangId] [int] NULL,
	[UISkinId] [int] NULL,
 CONSTRAINT [PK_BetTransaction] PRIMARY KEY CLUSTERED 
(
	[BetTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CashoutHistory]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashoutHistory](
	[BonusPartBase] [decimal](19, 10) NULL,
	[WinningsBase] [decimal](19, 10) NULL,
	[PotWinningsBase] [decimal](19, 10) NULL,
	[TotalStakeBase] [decimal](19, 10) NULL,
	[CurrencyRate] [decimal](19, 10) NULL,
	[SettlementDate] [datetime] NULL,
	[MultipleBonusAmount] [decimal](19, 10) NULL,
	[BetId] [int] NOT NULL,
	[Winnings] [decimal](19, 10) NULL,
	[CreationDate] [datetime] NULL,
	[Status] [int] NULL,
	[Amount] [decimal](19, 10) NULL,
	[BonusPart] [decimal](19, 10) NULL,
	[TotalStake] [decimal](19, 10) NULL,
	[UnitStake] [decimal](19, 10) NULL,
	[CashoutHistoryId] [int] NOT NULL,
	[PotWinnings] [decimal](19, 10) NULL,
 CONSTRAINT [PK_CashoutHistory] PRIMARY KEY CLUSTERED 
(
	[CashoutHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Player]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Player](
	[IsVip] [bit] NULL,
	[MultipleBonusesId] [int] NULL,
	[Color] [int] NULL,
	[PlayerLimitGroupId] [int] NULL,
	[SysParameterSetId] [int] NULL,
	[LoginId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Name] [nvarchar](max) NULL,
	[SkinId] [int] NULL,
	[AffiliateId] [int] NULL,
	[PlayerId] [int] NOT NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PlayerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regulators]    Script Date: 26.08.2021 10:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regulators](
	[RegulatorId] [int] NOT NULL,
	[BetId] [int] NOT NULL,
 CONSTRAINT [PK_Regulators] PRIMARY KEY CLUSTERED 
(
	[RegulatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BetBuilderSelections]  WITH CHECK ADD  CONSTRAINT [FK_BetBuilderSelections_BetSelections] FOREIGN KEY([BetSelectionId])
REFERENCES [dbo].[BetSelections] ([BetSelectionId])
GO
ALTER TABLE [dbo].[BetBuilderSelections] CHECK CONSTRAINT [FK_BetBuilderSelections_BetSelections]
GO
ALTER TABLE [dbo].[Bets]  WITH CHECK ADD  CONSTRAINT [FK_Bets_BetTransaction] FOREIGN KEY([BetTransactionId])
REFERENCES [dbo].[BetTransaction] ([BetTransactionId])
GO
ALTER TABLE [dbo].[Bets] CHECK CONSTRAINT [FK_Bets_BetTransaction]
GO
ALTER TABLE [dbo].[Bets]  WITH CHECK ADD  CONSTRAINT [FK_Bets_Player] FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Player] ([PlayerId])
GO
ALTER TABLE [dbo].[Bets] CHECK CONSTRAINT [FK_Bets_Player]
GO
ALTER TABLE [dbo].[BetSelections]  WITH CHECK ADD  CONSTRAINT [FK_BetSelections_Bets] FOREIGN KEY([BetId])
REFERENCES [dbo].[Bets] ([BetId])
GO
ALTER TABLE [dbo].[BetSelections] CHECK CONSTRAINT [FK_BetSelections_Bets]
GO
ALTER TABLE [dbo].[CashoutHistory]  WITH CHECK ADD  CONSTRAINT [FK_CashoutHistory_Bets] FOREIGN KEY([BetId])
REFERENCES [dbo].[Bets] ([BetId])
GO
ALTER TABLE [dbo].[CashoutHistory] CHECK CONSTRAINT [FK_CashoutHistory_Bets]
GO
ALTER TABLE [dbo].[Regulators]  WITH CHECK ADD  CONSTRAINT [FK_Regulators_Bets] FOREIGN KEY([BetId])
REFERENCES [dbo].[Bets] ([BetId])
GO
ALTER TABLE [dbo].[Regulators] CHECK CONSTRAINT [FK_Regulators_Bets]
GO
