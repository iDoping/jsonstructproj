﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StructureProject.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class structureProjectEntities : DbContext
    {
        public structureProjectEntities()
            : base("name=structureProjectEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BetBuilderSelections> BetBuilderSelections { get; set; }
        public virtual DbSet<Bets> Bets { get; set; }
        public virtual DbSet<BetSelections> BetSelections { get; set; }
        public virtual DbSet<BetTransaction> BetTransaction { get; set; }
        public virtual DbSet<CashoutHistory> CashoutHistory { get; set; }
        public virtual DbSet<Player> Player { get; set; }
        public virtual DbSet<Regulators> Regulators { get; set; }
    }
}
