//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StructureProject.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bets
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bets()
        {
            this.BetSelections = new HashSet<BetSelections>();
            this.CashoutHistory = new HashSet<CashoutHistory>();
            this.Regulators = new HashSet<Regulators>();
        }
    
        public Nullable<bool> ReSettle { get; set; }
        public Nullable<bool> ManualSettle { get; set; }
        public Nullable<decimal> Bonus { get; set; }
        public Nullable<bool> SkipLimits { get; set; }
        public Nullable<int> BetProduct { get; set; }
        public Nullable<decimal> BonusPart { get; set; }
        public Nullable<int> SettlementStatus { get; set; }
        public Nullable<int> SelectionsCount { get; set; }
        public Nullable<int> FeedSourceId { get; set; }
        public Nullable<int> BonusMode { get; set; }
        public Nullable<decimal> TotalStakeBase { get; set; }
        public Nullable<decimal> PotWinBase { get; set; }
        public Nullable<bool> AutoSettle { get; set; }
        public Nullable<decimal> RealWinBase { get; set; }
        public Nullable<bool> HasPartialCashout { get; set; }
        public Nullable<int> TaxType { get; set; }
        public Nullable<decimal> TaxValue { get; set; }
        public int BetTransactionId { get; set; }
        public Nullable<decimal> BonusPartBase { get; set; }
        public Nullable<decimal> RealWin { get; set; }
        public int BetId { get; set; }
        public int PlayerId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> BetStatus { get; set; }
        public Nullable<int> BetType { get; set; }
        public Nullable<decimal> UnitStake { get; set; }
        public Nullable<decimal> TotalStake { get; set; }
        public Nullable<decimal> OpenStake { get; set; }
        public Nullable<decimal> PotWin { get; set; }
        public Nullable<decimal> OpenPotWin { get; set; }
        public Nullable<decimal> MaxWinnings { get; set; }
        public Nullable<int> EventsCount { get; set; }
        public Nullable<System.DateTime> SettlementDate { get; set; }
        public Nullable<int> CurrencyId { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> LicenseeId { get; set; }
    
        public virtual BetTransaction BetTransaction { get; set; }
        public virtual Player Player { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BetSelections> BetSelections { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CashoutHistory> CashoutHistory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Regulators> Regulators { get; set; }
    }
}
