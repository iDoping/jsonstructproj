﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using StructureProject.TempM;
using StructureProject.Model;
using NLog;

namespace StructureProject
{
    class Program
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            try
            {
                _logger.Info("Programm has been started");
                using (structureProjectEntities context = new structureProjectEntities())
                {
                    var perem = JsonConvert.DeserializeObject<List<Bett>>(File.ReadAllText("develop_bets_10k.json"));
                    var result = perem.Select(x => new Bets
                    {
                        ReSettle = x.ReSettle,
                        ManualSettle = x.ManualSettle,
                        Bonus = x.Bonus,
                        SkipLimits = x.SkipLimits,
                        BetProduct = x.BetProduct,
                        BonusPart = x.BonusPart,
                        SettlementStatus = x.SettlementStatus,
                        SelectionsCount = x.SelectionsCount,
                        FeedSourceId = x.FeedSourceId,
                        BonusMode = x.BonusMode,
                        TotalStakeBase = x.TotalStakeBase,
                        PotWinBase = x.PotWinBase,
                        AutoSettle = x.AutoSettle,
                        RealWinBase = x.RealWinBase,
                        HasPartialCashout = x.HasPartialCashout,
                        TaxType = x.TaxType,
                        TaxValue = x.TaxValue,
                        BetTransactionId = x.BetTransactionId,
                        BetTransaction = new BetTransaction
                        {
                            BetTransactionId = x.BetTransaction.BetTransactionId,
                            BetsCount = x.BetTransaction.BetsCount,
                            CreatedDate = x.BetTransaction.CreatedDate,
                            BonusMeta = JsonConvert.SerializeObject(x.BetTransaction.BonusMeta),
                            ClientIp = x.BetTransaction.ClientIp,
                            Device = x.BetTransaction.Device,
                            LangId = x.BetTransaction.LangId,
                            UISkinId = x.BetTransaction.UISkinId,
                        },
                        Player = new Player
                        {
                            IsVip = x.Player.IsVip,
                            MultipleBonusesId = x.Player.MultipleBonusesId,
                            Color = x.Player.Color,
                            PlayerLimitGroupId = x.Player.PlayerLimitGroupId,
                            SysParameterSetId = x.Player.SysParameterSetId,
                            LoginId = x.Player.LoginId,
                            CreatedDate = x.Player.CreatedDate,
                            Name = x.Player.Name,
                            SkinId = x.Player.SkinId,
                            AffiliateId = x.Player.AffiliateId,
                            PlayerId = x.Player.PlayerId
                        },
                        BetSelections = x.BetSelections.Select(b => new BetSelections
                        {
                            ExistBetsValue = b.ExistBetsValue,
                            SelectionTypeId = b.SelectionTypeId,
                            SportMarketId = b.SelectionTypeId,
                            SportId = b.SportId,
                            SportName = b.SportName,
                            CategoryId = b.CategoryId,
                            ChampId = b.ChampId,
                            MarketName = b.MarketName,
                            IsLive = b.IsLive,
                            IsVirtual = b.IsVirtual,
                            State = b.State,
                            IsBetBuilder = b.IsBetBuilder,
                            IsParlay = b.IsParlay,
                            IsLiveStream = b.IsLiveStream,
                            FeedSourceId = b.FeedSourceId,
                            DbId = b.DbId,
                            VSportGameId = b.VSportGameId,
                            EventFactorValue = b.EventFactorValue,
                            EventDate = b.EventDate,
                            SettlOt = b.SettlOt,
                            BetSelectionId = b.BetSelectionId,
                            BetId = b.BetId,
                            IsBanker = b.IsBanker,
                            BetProduct = b.BetProduct,
                            FeedSelectionId = b.FeedSelectionId,
                            FeedEventId = b.FeedEventId,
                            IntEventId = b.IntEventId,
                            Price = b.Price,
                            PriceType = b.PriceType,
                            Status = b.Status,
                            MarketType = b.MarketType,
                            EventDesc = b.EventDesc,
                            ChampDesc = b.ChampDesc,
                            CatDesc = b.CatDesc,
                            SelectionName = b.SelectionName,
                            SelectionCode = b.SelectionCode,
                            Spec = b.Spec,
                            FeedMarketId = b.FeedMarketId,
                            BetBuilderSelections = b.BetBuilderSelections.Select(be => new BetBuilderSelections
                            {
                            }).ToList()
                        }).ToList(),
                        BonusPartBase = x.BonusPartBase,
                        RealWin = x.RealWin,
                        BetId = x.BetId,
                        PlayerId = x.PlayerId,
                        CreatedDate = x.CreatedDate,
                        BetStatus = x.BetStatus,
                        BetType = x.BetType,
                        UnitStake = x.UnitStake,
                        TotalStake = x.TotalStake,
                        OpenStake = x.OpenStake,
                        PotWin = x.PotWin,
                        OpenPotWin = x.OpenPotWin,
                        MaxWinnings = x.MaxWinnings,
                        EventsCount = x.EventsCount,
                        SettlementDate = x.SettlementDate,
                        CurrencyId = x.CurrencyId,
                        CurrencyRate = x.CurrencyRate,
                        IsLocked = x.IsLocked,
                        ModifiedDate = x.ModifiedDate,
                        CashoutHistory = x.CashoutHistory.Select(c => new CashoutHistory
                        {
                            BonusPartBase = c.BonusPartBase,
                            WinningsBase = c.WinningsBase,
                            PotWinningsBase = c.PotWinningsBase,
                            TotalStakeBase = c.TotalStakeBase,
                            CurrencyRate = c.CurrencyRate,
                            SettlementDate = c.SettlementDate,
                            MultipleBonusAmount = c.MultipleBonusAmount,
                            BetId = c.BetId,
                            Winnings = c.Winnings,
                            CreationDate = c.CreationDate,
                            Status = c.Status,
                            Amount = c.Amount,
                            BonusPart = c.BonusPart,
                            TotalStake = c.TotalStake,
                            UnitStake = c.UnitStake,
                            CashoutHistoryId = c.CashoutHistoryId,
                            PotWinnings = c.PotWinnings
                        }).ToList(),
                        LicenseeId = x.LicenseeId,
                        Regulators = x.Regulators.Select(r => new Regulators
                        { }).ToList(),
                    }).ToList();

                    var conditionBetTransaction = new HashSet<int>();
                    var conditionPlayer = new HashSet<int>();

                    foreach (var res in result)
                    {
                        if (!conditionBetTransaction.Contains(res.BetTransaction.BetTransactionId))
                        {
                            conditionBetTransaction.Add(res.BetTransaction.BetTransactionId);
                        }
                        else
                        {
                            res.BetTransaction = null;
                        }

                        if (!conditionPlayer.Contains(res.Player.PlayerId))
                        {
                            conditionPlayer.Add(res.Player.PlayerId);
                        }
                        else
                        {
                            res.Player = null;
                        }
                    }
                    context.Bets.AddRange(result);
                    context.SaveChanges();
                }
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
#if DEBUG
                _logger.Error(ex, "Error was occured during add record into database");
#else
                 _logger.Error("Error was occured during add record into database");
#endif
            }
            catch (Exception ex)
            {
#if DEBUG
                _logger.Error(ex);
#else
                _logger.Error("Error was occured during the executing programm");
#endif
            }
            Console.ReadKey();
        }
    }
}