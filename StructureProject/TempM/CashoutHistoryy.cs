
namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;
    
    public partial class CashoutHistoryy
    {
        public Nullable<decimal> BonusPartBase { get; set; }
        public Nullable<decimal> WinningsBase { get; set; }
        public Nullable<decimal> PotWinningsBase { get; set; }
        public Nullable<decimal> TotalStakeBase { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public Nullable<System.DateTime> SettlementDate { get; set; }
        public Nullable<decimal> MultipleBonusAmount { get; set; }
        public int BetId { get; set; }
        public Nullable<decimal> Winnings { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> BonusPart { get; set; }
        public Nullable<decimal> TotalStake { get; set; }
        public Nullable<decimal> UnitStake { get; set; }
        public int CashoutHistoryId { get; set; }
        public Nullable<decimal> PotWinnings { get; set; }
    
        public virtual Bett Bets { get; set; }
    }
}
