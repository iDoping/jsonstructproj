
namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Playerr
    {        
        public Nullable<bool> IsVip { get; set; }
        public Nullable<int> MultipleBonusesId { get; set; }
        public Nullable<int> Color { get; set; }
        public Nullable<int> PlayerLimitGroupId { get; set; }
        public Nullable<int> SysParameterSetId { get; set; }
        public Nullable<int> LoginId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Name { get; set; }
        public Nullable<int> SkinId { get; set; }
        public Nullable<int> AffiliateId { get; set; }
        public int PlayerId { get; set; }
        public virtual ICollection<Bett> Bets { get; set; }
    }
}
