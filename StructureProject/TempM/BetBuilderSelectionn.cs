namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;

    public partial class BetBuilderSelectionn
    {
        public int BetBuilderSelectionId { get; set; }
        public int BetSelectionId { get; set; }

        public virtual BetSelectionn BetSelections { get; set; }
    }
}
