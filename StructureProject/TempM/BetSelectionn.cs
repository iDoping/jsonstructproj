
namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;

    public partial class BetSelectionn
    {      
        public Nullable<decimal> ExistBetsValue { get; set; }
        public Nullable<int> SelectionTypeId { get; set; }
        public Nullable<int> SportMarketId { get; set; }
        public Nullable<int> SportId { get; set; }
        public string SportName { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> ChampId { get; set; }
        public string MarketName { get; set; }
        public Nullable<bool> IsLive { get; set; }
        public Nullable<bool> IsVirtual { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<bool> IsBetBuilder { get; set; }
        public Nullable<bool> IsParlay { get; set; }
        public Nullable<bool> IsLiveStream { get; set; }
        public Nullable<int> FeedSourceId { get; set; }
        public Nullable<int> DbId { get; set; }
        public Nullable<int> VSportGameId { get; set; }
        public Nullable<decimal> EventFactorValue { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public Nullable<bool> SettlOt { get; set; }
        public int BetSelectionId { get; set; }
        public int BetId { get; set; }
        public Nullable<bool> IsBanker { get; set; }
        public Nullable<int> BetProduct { get; set; }
        public Nullable<int> FeedSelectionId { get; set; }
        public Nullable<int> FeedEventId { get; set; }
        public Nullable<int> IntEventId { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> PriceType { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> MarketType { get; set; }
        public string EventDesc { get; set; }
        public string ChampDesc { get; set; }
        public string CatDesc { get; set; }
        public string SelectionName { get; set; }
        public string SelectionCode { get; set; }
        public string Spec { get; set; }
        public Nullable<int> FeedMarketId { get; set; }
        public virtual ICollection<BetBuilderSelectionn> BetBuilderSelections { get; set; }
        public virtual Bett Bets { get; set; }
    }
}
