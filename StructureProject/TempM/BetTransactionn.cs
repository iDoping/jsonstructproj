
namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;
    
    public partial class BetTransactionn
    {     
        public int BetTransactionId { get; set; }
        public Nullable<int> BetsCount { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public MetaInformation BonusMeta { get; set; }
        public string ClientIp { get; set; }
        public Nullable<int> Device { get; set; }
        public Nullable<int> LangId { get; set; }
        public Nullable<int> UISkinId { get; set; }
    
        public virtual ICollection<Bett> Bets { get; set; }
    }

    public class MetaInformation    {
        public int BonusTemplateId { get; set; }
        public bool BonusNetWins { get; set; }
        public decimal MinBonusPrice { get; set; }
        public ICollection<Bonuses> Bonuses { get; set; }
        public ICollection<long> ExcludedBonusMarkets { get; set; }
        public ICollection<int> ExcludedBonusSports { get; set; }
        public  decimal? MaxExtraWinnings { get; set; }
    }

    public class Bonuses
    {
    public decimal BonusValue { get; set; }
    public int LowBound { get; set; }
    public int TopBound { get; set; }
    }
}
