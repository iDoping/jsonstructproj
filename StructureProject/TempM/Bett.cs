namespace StructureProject.TempM
{
    using System;
    using System.Collections.Generic;

    public partial class Bett
    {     
        public Nullable<bool> ReSettle { get; set; }
        public Nullable<bool> ManualSettle { get; set; }
        public Nullable<decimal> Bonus { get; set; }
        public Nullable<bool> SkipLimits { get; set; }
        public Nullable<int> BetProduct { get; set; }
        public Nullable<decimal> BonusPart { get; set; }
        public Nullable<int> SettlementStatus { get; set; }
        public Nullable<int> SelectionsCount { get; set; }
        public Nullable<int> FeedSourceId { get; set; }
        public Nullable<int> BonusMode { get; set; }
        public Nullable<decimal> TotalStakeBase { get; set; }
        public Nullable<decimal> PotWinBase { get; set; }
        public Nullable<bool> AutoSettle { get; set; }
        public Nullable<decimal> RealWinBase { get; set; }
        public Nullable<bool> HasPartialCashout { get; set; }
        public Nullable<int> TaxType { get; set; }
        public Nullable<decimal> TaxValue { get; set; }
        public int BetTransactionId { get; set; }
        public Nullable<decimal> BonusPartBase { get; set; }
        public Nullable<decimal> RealWin { get; set; }
        public int BetId { get; set; }
        public int PlayerId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> BetStatus { get; set; }
        public Nullable<int> BetType { get; set; }
        public Nullable<decimal> UnitStake { get; set; }
        public Nullable<decimal> TotalStake { get; set; }
        public Nullable<decimal> OpenStake { get; set; }
        public Nullable<decimal> PotWin { get; set; }
        public Nullable<decimal> OpenPotWin { get; set; }
        public Nullable<decimal> MaxWinnings { get; set; }
        public Nullable<int> EventsCount { get; set; }
        public Nullable<System.DateTime> SettlementDate { get; set; }
        public Nullable<int> CurrencyId { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> LicenseeId { get; set; }

        public virtual BetTransactionn BetTransaction { get; set; }
        public virtual Playerr Player { get; set; }      
        public virtual ICollection<BetSelectionn> BetSelections { get; set; }
        public virtual ICollection<CashoutHistoryy> CashoutHistory { get; set; }
        public virtual ICollection<Regulatorr> Regulators { get; set; }
    }
}
